import React from 'react';
import ReactDOM from 'react-dom';
import App from './screens/App';
import {BrowserRouter} from 'react-router-dom';

import './utils/dataAdapters';

import 'normalize.css';
import './index.css';


ReactDOM.render(<BrowserRouter><App /></BrowserRouter>, document.getElementById('root'));
