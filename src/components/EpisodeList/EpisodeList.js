import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './EpisodeList.css';

class EpisodeList extends Component {
  render() {
    const { selectedSeason, episodes, onAddToFavorite } = this.props;

    return (
      <div className="episode-list">
        {selectedSeason && <h4>Season {selectedSeason.number} episodes</h4>}
        <table className="episode-list--table">
          <thead>
            <tr>
              <td>Number</td>
              <td>Date</td>
              <td>Name</td>
              <td>Favorite</td>
            </tr>
          </thead>
          <tbody>
          {episodes.map((episode) => {
            let {number, airdate, name, season} = episode,
              favoriteClassName = episode.isFavorite ? 'btn--add-to-favorites__favorite' : '';

            airdate = new Date(airdate).toDateString();
            return <tr key={number}>
              <td>{number}</td>
              <td>{airdate}</td>
              <td><Link to={`/season/${season}/episode/${number}`}>{name}</Link></td>
              <td><button onClick={() => { onAddToFavorite(episode) }}
                          class={`btn btn--add-to-favorites ${favoriteClassName}`}>♥</button>
              </td>
            </tr>
          })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default EpisodeList;
