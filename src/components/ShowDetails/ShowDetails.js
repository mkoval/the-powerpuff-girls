import React, { Component } from 'react';

import './ShowDetails.css';

class ShowMain extends Component {
  render() {
    const { image, summary, rating, name } = this.props.showDetails;

    return (
      <div className="show-details">
        <div className="show-details--image">
          <img src={image && image.medium} alt={name} />
          <span className="show-details--rating">Rating: {rating && rating.average}</span>
        </div>
        <div className="show-details--summary" dangerouslySetInnerHTML={{__html: summary}} />
      </div>
    );
  }
}

export default ShowMain;
