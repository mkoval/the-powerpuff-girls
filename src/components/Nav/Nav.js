import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './Nav.css';

class Nav extends Component {
  render() {

    return (
      <nav className="app-nav">
        <div className="app-nav--menu">
          <Link to="/" className="app-nav--btn">Main</Link>
          <Link to="/season/1/episodes" className="app-nav--btn">Episodes list</Link>
          <Link to="/favorites" className="app-nav--btn">Favorite episodes</Link>
        </div>
      </nav>
    );
  }
}

export default Nav;
