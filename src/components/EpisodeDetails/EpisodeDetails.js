import React, { Component } from 'react';

import './EpisodeDetails.css';

class EpisodeDetails extends Component {
  render() {
    const { image, summary, name } = this.props.episodeDetails;

    return (
      <div className="episode-details">
        <div className="episode-details--image">
          <img src={image && image.medium} alt={name} />
        </div>
        <div className="episode-details--summary" dangerouslySetInnerHTML={{__html: summary}} />
      </div>
    );
  }
}

export default EpisodeDetails;
