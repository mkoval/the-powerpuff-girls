import React, { Component } from 'react';
import {Link} from 'react-router-dom';

import './SeasonsNav.css';

class SeasonsNav extends Component {
  render() {
    const { seasons } = this.props;

    return (
      <div className="seasons-nav">
        <span className="seasons-nav--title"> Seasons: </span>
        <ul className="seasons-nav--list">
          {seasons.map( (season) => {
            return <li key={season.number}><Link to={`/season/${season.number}/episodes`}>{season.number}</Link></li>
          } )}
        </ul>
      </div>
    );
  }
}

export default SeasonsNav;
