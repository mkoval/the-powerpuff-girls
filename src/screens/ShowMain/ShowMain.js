import React, { Component } from 'react';
import ShowDetails from '../../components/ShowDetails/ShowDetails';

import DataAdapter from '../../utils/DataAdapter/DataAdapter';

import './ShowMain.css';

class ShowMain extends Component {
  state = {
    showDetails: {},
    loading: true
  };

  componentDidMount = () => {
    const { showId } = this.props;
    
    DataAdapter.fetch(`/shows/${showId}/`)
      .then((res) => {
        this.setState({showDetails: res});
      })
      .finally(() => {
        this.setState({loading: false});
      })
  };

  render() {
    const { showDetails, loading } = this.state,
      { name } = showDetails;

    return (
      <React.Fragment>
        <div className="show-main" data-loading={loading}>
          <h2>{name}</h2>
          <ShowDetails showDetails={showDetails}/>
        </div>
      </React.Fragment>
    );
  }
}

export default ShowMain;
