import React, { Component } from 'react';
import EpisodeDetails from '../../components/EpisodeDetails/EpisodeDetails';

import DataAdapter from '../../utils/DataAdapter/DataAdapter';

import './EpisodeMain.css';

class EpisodeMain extends Component {
  state = {
    episodeDetails: {},
    loading: true
  };

  componentDidMount = () => {
    const { showId, seasonNum, episodeNum } = this.props;
    
    DataAdapter.fetch(`/${showId}/season/${seasonNum}/episode/${episodeNum}/`)
      .then((res) => {
        this.setState({episodeDetails: res});
      })
      .finally(() => {
        this.setState({loading: false});
      })
  };

  render() {
    const { episodeDetails, loading } = this.state,
      { name, number } = episodeDetails;

    return (
      <React.Fragment>
        <div className="show-main" data-loading={loading}>
          <h2>{number}: {name}</h2>
          <EpisodeDetails episodeDetails={episodeDetails} />
        </div>
      </React.Fragment>
    );
  }
}

export default EpisodeMain;
