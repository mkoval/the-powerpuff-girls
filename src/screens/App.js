import React, {Component} from 'react';
import {Route} from 'react-router-dom';

import Nav from '../components/Nav/Nav';
import ShowMain from './ShowMain/ShowMain';
import ShowEpisodes from './ShowEpisodes/ShowEpisodes';
import EpisodeMain from './EpisodeMain/EpisodeMain';
import FavoriteEpisodes from './FavoriteEpisodes/FavoriteEpisodes';

import './App.css';

const SHOW_ID = 6771;

class App extends Component {
  render() {
    return (
      <div className="app">
        <Nav />

        <Route exact path="/"
            render={()=> (
              <ShowMain showId={SHOW_ID}/>
            )}
        />

        <Route exact path="/season/:season/episodes"
            render={({ match })=> (
              <ShowEpisodes showId={SHOW_ID} seasonNum={match.params.season}/>
            )}
        />

        <Route exact path="/season/:season/episode/:episode"
             render={({ match })=> (
               <EpisodeMain showId={SHOW_ID} seasonNum={match.params.season} episodeNum={match.params.episode}/>
             )}
        />

        <Route exact path="/favorites"
             render={()=> (
               <FavoriteEpisodes />
             )}
        />

      </div>
    );
  }
}

export default App;
