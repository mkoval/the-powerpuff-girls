import React, { Component } from 'react';

import EpisodeList from '../../components/EpisodeList/EpisodeList';
import SeasonsNav from '../../components/SeasonsNav/SeasonsNav';

import DataAdapter from '../../utils/DataAdapter/DataAdapter';


class ShowEpisodes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showDetails: {},
      loading: true,
      seasons: [],
      episodes: [],
      favorites: []
    }
  }

  componentDidMount () {
    const { showId } = this.props,
      show = DataAdapter.fetch(`/shows/${showId}/`),
      seasons = DataAdapter.fetch(`/shows/${showId}/seasons/`),
      favorites = DataAdapter.fetch(`/favorites/`);

    Promise.all([show, seasons, favorites])
      .then((values) => {
        const [showDetails, seasons, favorites ] = values;
        this.setState({showDetails, seasons, favorites});
        return seasons;
      })
      .then((seasons) => {
        const selectedSeasonInfo = this.getSelectedSeasonInfo(this.props.seasonNum, seasons),
          selectedSeasonId = selectedSeasonInfo.id;

        return DataAdapter.fetch(`/seasons/${selectedSeasonId}/episodes/`)
      })
      .then((episodes) => {
        const {favorites} = this.state;
        episodes = this.processFavoriteEpisodes(episodes, favorites);
        this.setState({episodes});
      })
      .finally(() => {
        this.setState({loading: false})
      });
  };

  loadedSelectedSeasonEpisodes = (selectedSeason) => {
    const { seasons } = this.state,
      selectedSeasonInfo = this.getSelectedSeasonInfo(selectedSeason, seasons),
      selectedSeasonId = selectedSeasonInfo.id;

    DataAdapter
      .fetch(`/seasons/${selectedSeasonId}/episodes`)
      .then((episodes) => {
        const {favorites} = this.state;
        episodes = this.processFavoriteEpisodes(episodes, favorites);
        this.setState({episodes});
      })
  };

  processFavoriteEpisodes = (episodes, favorites) => {
    return episodes.map((episode) => {
      episode.isFavorite = favorites.some((favSeason) => {return episode.id === favSeason.id});
      return episode;
    });
  };

  getSelectedSeasonInfo = (selectedSeasonNumber, seasons) => {
      selectedSeasonNumber = parseInt(selectedSeasonNumber, 10);
      return seasons.find( (season) => { return season.number === selectedSeasonNumber});
  };

  onAddToFavorite = (episode) => {
    const isFavorite = episode.isFavorite;
    let { favorites } = this.state;

    if (isFavorite) {
      favorites = favorites.filter( (favEpisode) => {
        return favEpisode.id !== episode.id;
      })
    } else {
      episode.isFavorite = true;
      favorites.push(episode)
    }

    DataAdapter
      .fetch(`/favorites/`, {method: 'POST', body: favorites})
      .then((favorites) => {
        this.setState({favorites});
      })
  };

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.seasonNum !== this.props.seasonNum) {
      this.loadedSelectedSeasonEpisodes(this.props.seasonNum)
    }
  }

  render() {
    const { seasons, episodes, loading} = this.state,
      selectedSeasonInfo = this.getSelectedSeasonInfo(this.props.seasonNum, seasons),
      { name } = this.state.showDetails;

    return (
      <React.Fragment>
        <div className="show-episodes" data-loading={loading}>
          <h2>{`${name} - episodes`}</h2>
          <SeasonsNav seasons={seasons} />
          {selectedSeasonInfo && <EpisodeList onAddToFavorite={this.onAddToFavorite} selectedSeason={selectedSeasonInfo} episodes={episodes}/>}
        </div>
      </React.Fragment>
    );
  }
}

export default ShowEpisodes;
