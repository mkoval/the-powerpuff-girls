import React, { Component } from 'react';

import EpisodeList from '../../components/EpisodeList/EpisodeList';
import SeasonsNav from '../../components/SeasonsNav/SeasonsNav';

import DataAdapter from '../../utils/DataAdapter/DataAdapter';


class FavoriteEpisodes extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      favorites: []
    }
  }

  componentDidMount () {
    DataAdapter.fetch(`/favorites/`)
      .then((favorites) => {
        this.setState({favorites});
      })
      .finally(() => {
        this.setState({loading: false})
      });
  };

  loadedSelectedSeasonEpisodes = (selectedSeason) => {
    const { seasons } = this.state,
      selectedSeasonInfo = this.getSelectedSeasonInfo(selectedSeason, seasons),
      selectedSeasonId = selectedSeasonInfo.id;

    DataAdapter
      .fetch(`/seasons/${selectedSeasonId}/episodes`)
      .then((episodes) => {
        const {favorites} = this.state;
        episodes = this.processFavoriteEpisodes(episodes, favorites);
        this.setState({episodes});
      })
  };

  onAddToFavorite = (episode) => {
    let { favorites } = this.state;

    favorites = favorites.filter( (favEpisode) => {
      return favEpisode.id !== episode.id;
    });

    DataAdapter
      .fetch(`/favorites/`, {method: 'POST', body: favorites})
      .then((favorites) => {
        this.setState({favorites});
      })
  };

  render() {
    const { favorites, loading} = this.state;
    return (
      <React.Fragment>
        <div className="favorite-episodes" data-loading={loading}>
          <h2>Favorite episodes</h2>
          <EpisodeList onAddToFavorite={this.onAddToFavorite} episodes={favorites}/>
        </div>
      </React.Fragment>
    );
  }
}

export default FavoriteEpisodes;
