/**
 * @namespace
 */
class Transport {
    
  /**
   * Standard window.fetch 
   * 
   * @param {string} uri
   * @param {object} options
   * @returns {Promise}
   */
    static fetch = (uri, options)=> {
        return window.fetch(uri, options);
    };

    /**
     * Implements window.fetch interface for jsonp
     *
     * @param {string} uri
     * @param {object} options
     * @returns {Promise}
     */
    static jsonp = (uri, {method = 'GET'}) => {
        method = method.toUpperCase();
        
        const promise =  new Promise(function(resolve, reject) {
            const id = '_' + Math.round(10000 * Math.random()),
                callbackName = 'jsonp_callback_' + id,
                src = uri + '&callback=' + callbackName,
                script = document.createElement('script');

            if (method !== 'GET') {
                reject({status: 'error', message: `Transport.jsonp doesn't support method ${method}`});
                return;
            }

            script.src = src;
            script.id = id;

            script.addEventListener('error', (e) => {
                reject({status: 'error', message: e})
            });

            (document.getElementsByTagName('head')[0] || document.body || document.documentElement).appendChild(script)

            window[callbackName] = function(data) {
                const script = document.getElementById(id);
                delete window[callbackName];
                script.parentNode.removeChild(script);
                resolve(data);
            }
        });

        return promise;
    };

    /**
     * Implements window.fetch interface for localStorage
     *
     * @param {string} uri
     * @param {object} options
     * @returns {Promise}
     */
    static localStorage = (uri, {method = 'GET', body = {}}) => {
        method = method.toUpperCase();

        const myStorage = window.localStorage,
            promise = new Promise((resolve, reject) => {
                switch (method){
                    case "POST":
                        if (typeof body === 'object') {
                            try {
                                body = JSON.stringify(body)
                            } catch (e) {
                                reject({ok: false, message: 'Transport.localStorage: body can\'t be strignify'})
                                return;
                            }

                            try {
                                myStorage.setItem(uri, body);
                            } catch (e) {
                                reject({ok: false, message: e});
                                return;
                            }
                        }

                        resolve({ok: true, data: JSON.parse(body)});
                        break;
                    case "GET":
                        let response;

                        try {
                            response = myStorage.getItem(uri);
                        } catch (e) {
                            reject({ok: false, message: e});
                            return;
                        }

                        resolve({ok: true, data: JSON.parse(response || '[]')});
                        break;
                    default:
                        reject({ok: false, message: `Transport.localStorage: Method ${method} is not implemented`});
                }
            });

        return promise;
    }
}

export default Transport