import DataAdapter from './DataAdapter/DataAdapter';
import Transport from './Transport/Transport';

const baseURL = "http://api.tvmaze.com";

function ShowMainPreprocessor(response) {
  return response.json().then((json) => {
    return json
  });
}

DataAdapter.register('/shows/([0-9]+)/?', {
  map: `${baseURL}/shows/$1`,
  transport: Transport.fetch,
  preprocessor: ShowMainPreprocessor,
  defReqOptions: {mode: 'cors'}
});

DataAdapter.register('/seasons/([0-9]+)/episodes/?', {
  map: `${baseURL}/seasons/$1/episodes`,
  transport: Transport.fetch,
  preprocessor: ShowMainPreprocessor,
  defReqOptions: {mode: 'cors'}
});

DataAdapter.register('/shows/([0-9]+)/seasons/?', {
  map: `${baseURL}/shows/$1/seasons`,
  transport: Transport.fetch,
  preprocessor: ShowMainPreprocessor,
  defReqOptions: {mode: 'cors'}
});

DataAdapter.register('/([0-9]+)/season/([0-9]+)/episode/([0-9]+)/?', {
  map: `${baseURL}/shows/$1/episodebynumber?season=$2&number=$3`,
  transport: Transport.fetch,
  preprocessor: ShowMainPreprocessor,
  defReqOptions: {mode: 'cors'}
});

DataAdapter.register('/favorites/?', {
  transport: Transport.localStorage,
  defReqOptions: {method: 'GET'},
  preprocessor: (response) => {
    let json = {};
    if (response.ok) {
      json = response.data;
    }

    return json;
  }
});

